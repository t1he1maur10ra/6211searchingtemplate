﻿/*Template for the Linear and Binary Search Comparison*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searching
{
    class Program
    {
        static void Main(string[] args)
        {
            /*---Enter Code Here---*/
        }


        /*---Linear Search Algorithm---*/

        /*---Binary Search Algorithm---*/

        /*---Recursive Binary Search Algorithm---*/

        /*---Bubble Sort Algorithm---*/


        /*---Display Array Method---*/
        public static void Display(int[] arr)
        {
            for (int i = 1; i <= arr.Length; i++)
            {
                Console.Write(arr[i - 1] + " ");
                if (i % 10 == 0)                        //will only display 10 values per line.
                    Console.WriteLine();
            }
            Console.WriteLine();
        }

        /*---Populate Array Method---*/
        public static int[] Populate(int[] arr)
        {
            int min = 0;                                //Lowset possible number
            int max = 200;                              //Highest possible number
            Random randNum = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = randNum.Next(min, max);        //Assign the random number to the i indicies in the array
            }
            return arr;                                 //Return the populated array
        }
    }
}
